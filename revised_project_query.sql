select rv_projected_cost, rv_gob_part,rv_foreign_part,rv_other_part,rv_projected_expense_other_part 
from operational_project_info where is_revised=1

select id as nor_revised_p_id, projected_expense, projected_expense_gob,projected_expense_foreign,projected_expense_other,projected_expense_others_part 
from operational_project_info
where id in (select parent_project_id from operational_project_info where is_revised=1 and revised_number=2)

select id as first_revised_p_id, projected_expense, projected_expense_gob,projected_expense_foreign,projected_expense_other,projected_expense_others_part 
from operational_project_info
where is_revised = 1 and revised_number=2 and parent_project_id <>0


UPDATE operational_project_info
SET 
rv_other_part = projected_expense_other,
rv_projected_expense_other_part = projected_expense_others_part,
rv_foreign_part = projected_expense_foreign,
rv_gob_part = projected_expense_gob,
rv_projected_cost = projected_expense
where is_revised = 1 and revised_number=2 and parent_project_id <>0


UPDATE operational_project_info
SET 
projected_expense_other = subquery.projected_expense_other,
projected_expense_others_part = subquery.projected_expense_others_part,
projected_expense_foreign = subquery.projected_expense_foreign,
projected_expense_gob = subquery.projected_expense_gob,
projected_expense = subquery.projected_expense
from 
	(select id as first_revised_p_id, projected_expense, projected_expense_gob,projected_expense_foreign,projected_expense_other,projected_expense_others_part 
	from operational_project_info
	where is_revised = 1 and revised_number=1 and parent_project_id <>0) 
as subquery
	where is_revised = 1 and revised_number=1 and parent_project_id <>0


