INSERT INTO reporting.fct_project_granular_cost_summary (project_id,location_id,approval_date,implementing_org_id,sponsor_org_id,projected_expense,projected_expense_gob,projected_expense_foreign,projected_expense_other,projected_expense_revised,projected_expense_gob_revised,projected_expense_foreign_revised,projected_expense_other_revised,in_gob,in_pa,in_own,in_total,projected_expense_other_part,projected_expense_other_part_revised)
	(
		select prin.id,prin.id,(SELECT TO_DATE(prin.approval_date,'DD-MM-YYYY')),
		prin.id, prin.id,(select cast(prin.projected_expense as float)),
		(select cast(prin.projected_expense_gob as float)),
		(select cast(prin.projected_expense_foreign as float)),
		(select cast(prin.projected_expense_other as float)),
		(select cast(prin.rv_projected_cost as float)),
		(select cast(prin.rv_gob_part as float)),
		(select cast(prin.rv_foreign_part as float)),
		(select cast(prin.rv_other_part as float)),
		(case when is_revised >0 
		 	then (select cast(prin.rv_gob_part as float))-
		 		 (case when prin.revised_number>1 
						then (case when (select (select cast(parent.rv_gob_part as float)) 
							  from operational_project_info parent 
							  where parent.id= prin.parent_project_id) is null
							  	then 0 else (select (select cast(parent.rv_gob_part as float)) 
							  from operational_project_info parent 
							  where parent.id= prin.parent_project_id) end)
					 	else (select cast(prin.projected_expense_gob as float)) end)
		 	else 0 
		 end) as in_gob,
		(case when is_revised >0 
		 	then (select cast(prin.rv_foreign_part as float))-
		 		 (case when prin.revised_number>1 
						then (case when (select (select cast(parent.rv_foreign_part as float)) 
							  from operational_project_info parent 
							  where parent.id= prin.parent_project_id) is null
							  	then 0 else (select (select cast(parent.rv_foreign_part as float)) 
							  from operational_project_info parent 
							  where parent.id= prin.parent_project_id) end)
					 	else (select cast(prin.projected_expense_foreign as float)) end)
		 	else 0 end) as in_pa,
		(case when is_revised >0 
		 	then (select cast(prin.rv_other_part as float))-
				 (case when prin.revised_number>1 
						then (case when (select (select cast(parent.rv_other_part as float)) 
							  from operational_project_info parent 
							  where parent.id= prin.parent_project_id) is null
							  	then 0 else (select (select cast(parent.rv_other_part as float)) 
							  from operational_project_info parent 
							  where parent.id= prin.parent_project_id) end)
					 	else (select cast(prin.projected_expense_other as float)) end) 
		 	else 0 end) as in_own,
		(case when is_revised >0 
		 	then (select cast(prin.rv_projected_cost as float))-
				 (case when prin.revised_number>1 
						then (case when (select (select cast(parent.rv_projected_cost as float)) 
							  from operational_project_info parent 
							  where parent.id= prin.parent_project_id) is null
				  				then 0 else (select (select cast(parent.rv_projected_cost as float)) 
							  from operational_project_info parent 
							  where parent.id= prin.parent_project_id) end)
					 	else (select cast(prin.projected_expense as float)) end) 
		 	else 0 end) as in_total,
		(select cast(prin.projected_expense_others_part as float)),
		(select cast(prin.rv_projected_expense_other_part as float))
		from operational_project_info prin 
	) 
	on conflict (project_id) do nothing